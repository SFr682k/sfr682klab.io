from argparse import ArgumentParser
from datetime import date
from jinja2 import Environment, FileSystemLoader, select_autoescape
from jinja2.exceptions import TemplateNotFound
from os import mkdir, walk, path
from shutil import copytree, rmtree
from yaml import safe_load_all, YAMLError


cmdparser = ArgumentParser(description="Render page sources to static HTML")
cmdparser.add_argument('-u', dest='upload', action='store_true', help="Render for Upload to GitLab Pages")

cmdargs = cmdparser.parse_args()
upload = cmdargs.upload



env = Environment(
    loader = FileSystemLoader('src/templates/'),
    autoescape=select_autoescape(['html', 'xml'])
)

copyright_year = date.today().year


# Delete the output of previous renderings.
try:
    rmtree('www/')
except FileNotFoundError:
    pass


# Create a new output directory.
mkdir('www/')

# If no upload to GitLab: Copy images and static files to the output directory.
if not upload:
    copytree('img/', 'www/img/')
    copytree('static/', 'www/static/')


# Get a list of all files in src/data/.
file_list = []
for (dirpath, dirnames, filenames) in walk('src/data/'):
    file_list += [path.join(dirpath, file) for file in filenames]


for fname in file_list:
    with open(fname, 'r') as f:
        try:
            for obj in safe_load_all(f):
                if obj != None:
                    try:
                        # Obtain the template.
                        template = env.get_template(obj.pop('template'))
                        template.globals['copyright_year'] = copyright_year
                    except KeyError:
                        # Key 'template' missing in YAML file.
                        print("ERROR: Template name missing")
                        print("  occurred while processing file:", fname)
                        continue
                    except TemplateNotFound as exc:
                        # Template file not found.
                        print("ERROR: Template", str(obj['template']), "not found.")
                        print("  occurred while processing file:", fname)
                        continue
                    
                    try:
                        with open(path.join('www/', obj.pop('output')), 'w') as outfile:
                            # Render the template.  Items in obj, whose keys are neither
                            # 'template', nor 'output', are the template's context.
                            context = obj
                            outfile.write(template.render(context))
                    except KeyError:
                        print("ERROR: Output file name missing")
                        print("  occurred while processing file:", fname)
                        continue
                    
        except YAMLError as exc:
            print("Syntax Error:\n", exc)
